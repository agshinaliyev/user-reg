package problems.controller;


import problems.dto.UserLoginDto;
import problems.dto.UserRegisterDto;
import problems.dto.UserResetPasswordDto;
import org.springframework.web.bind.annotation.*;
import problems.service.UserService;

@RestController
@RequestMapping("/user_register")
public class UserController {

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping("/register")
    public void create(@RequestBody UserRegisterDto userRegisterDto) {

        userService.userRegister(userRegisterDto);

    }

    @PutMapping
    public void update(@RequestBody UserResetPasswordDto userResetPasswordDto){

        userService.resetPassword(userResetPasswordDto);
    }

    @GetMapping
    public String get(@RequestBody UserLoginDto userLoginDto) {

       return userService.userLogin(userLoginDto);
    }




}
