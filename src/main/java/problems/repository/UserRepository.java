package problems.repository;

import problems.model.User;
import org.springframework.data.jpa.repository.JpaRepository;


public interface UserRepository extends JpaRepository<User,Integer> {

     User getByUsername(String username);
     User getByEmail(String email);

}
